# Crawler

Zweck: Crawlen der Unternehmenswebsites.

## Allgemein

Benötigte Ressourcen: Datei mit Unternehmensdaten (im Unterverzeichnis 'resources') und WebDriver (im Verzeichnis 'crawler').

Skripte und Dateien:

| Datei | Nutzen |
| ------ | ------ |
| main_crawler.py | Setzt Parameter und managt Crawl-Prozess.|
| crawler.py | Crawlt für eine gegebene Website alle Webseiten, inklusive Subdomains.|
| robots.py | Prüft, ob URL gecrawlt werden darf.|
| formatter.py | Initiiert Parser und formatiert/speichert das Ergebnis.|
| parsing.py | Extrahiert Sätze, externe URLs und verlinkte Emails aus den Webseiten. Bereinigt/normalisiert Text (Codierungsfehler, Sonderzeichen, Zeichenwiederholungen).|
| ------ | ------ |
| save.data | Trackt letztes gecrawltes Unternehmen (Zeile der Eingabedatei) und gecrawlte Start-URLs. |

Hinweis:

- Wenn auf einer einzigen Webseite mehr als 1000 URLs gelistet sind, wird diese Webseite ignoriert.

## Abhängigkeiten

- langdetect
- lxml
- pandas
- requests
- selenium
- trafilatura

## Parameter

| Parameter | Standardwert | Bedeutung |
| ------ | ------ | ------ |
| timeout | 15 | Timeout in Sekunden für requests und selenium. |
| max_depth | -1 | Maximale Tiefe, negativer Wert für unbegrenzte Tiefe. Start-URL hat Tiefe 0, alle davon erreichbaren URLs haben Tiefe 1 usw. |
| limit | 250 | Maximale Anzahl zu crawlender Webseiten pro Website, negativer Wert für unbegrenztes Limit. Kurze URLs werden beim Überschreiten des Limits präferiert.|

# Database

Zweck: Schnittstelle zur MongoDB-Datenbank.

## Allgemein

Nach der Fertigstellung des Crawl-Prozesses können die Websites in die Datenbank eingelesen werden.

Skripte:

| Datei | Nutzen |
| ------ | ------ |
| write.py | Schreibt alle Daten aus dem Crawl-Prozess in die Datenbank. |
| read.py | Liest aus der Datenbank. |
| index.py | Manuelle Erstellung von Indizes, um den Leseprozess zu optimieren. |

## Abhängigkeiten

- pandas
- pymongo

# Extractor

Zweck: Bestimmung der Indikatoren.

## Allgemein

Ressourcen: Gecrawlte und geparste Websites aus der Datenbank.

Skripte und Dateien:

| Datei | Nutzen |
| ------ | ------ |
| main_extractor.py | Setzt Parameter und managt den Prozess. |
| extractor.py | Bestimmt die Indikatoren für alle Websites. |
| preprocessor.py | Textvorverarbeitung für Sätze und Keywords: Entfernung von Stoppwörtern, Grundformreduktion. |
| load_keywords.py | Lädt Keywords und Metadaten. |
| modify_output.py | Teilt Ausgabe-Datei nach Level (id und cluster) und Größe (je nach Parameter 'steps'). |
| ------ | ------ |
| save.data | Speichert alle bereits extrahierten Websites. |

Hinweis: Die Keyword-, Stoppwort- und Grundformlisten sind im Ordner 'lists' gespeichert. Die Stoppwort- und die Grundformliste stammt aus dem Projekt Deutscher Wortschatz<sup>[1](#myfootnote1)</sup>.

## Abhängigkeiten

- nltk
- rapidfuzz

## Parameter

| Parameter | Standardwert | Bedeutung |
| ------ | ------ | ------ |
| steps | [25, 50, 100, 200, 250] | Simuliert Crawl-Limits (bestimmt die Indikatoren nur aus den ersten x Webseiten). None wenn nicht gewünscht.|
| score_cutoff | 85 | Minimale Threshold (0 bis 100) für Mindestähnlichkeit.|

# Classifier

Zweck: Kategorisierung der Websites (Klassifikation und Regression).

## Allgemein

Ressourcen: Die Indikatoren, die zuvor aus den Websites bestimmt wurden.

Skripte:

| Datei | Nutzen |
| ------ | ------ |
| classifier.py | Klassifikation und Regression aller Websites. |
| evaluation.py | Evaluation der Modelle. |
| load.py | Lädt die aus den Websites ermittelten Indikatoren, die Keywords und die Trainingsdaten. |

Hinweis: Die Trainingsdaten befinden sich im Unterverzeichnis 'training'.

## Abhängigkeiten

- numpy
- sklearn

## Parameter

| Parameter | Standardwert | Bedeutung |
| ------ | ------ | ------ |
| excluded_codes | ['O', 'P', 'Q', 'unknown'] | WZ-Codes von Unternehmen, die nicht kategorisiert werden sollen.|
| level | None | Term-Ebene, entweder 'word', 'id' oder 'cluster'.|
| step | None | Anzahl der auszuwertenden Webseiten pro Website (Limit), abhängig von 'steps' (siehe Extractor).|

# Map

Zweck: Erstellung der Karten (Heatmaps und Markerkarten).

## Allgemein

Ressourcen: Kategorisierte Websites und eine GeoJSON-Datei für die Basemap.

Skripte:

| Datei | Nutzen |
| ------ | ------ |
| geocodes.py | Bestimmung der Längen- und Breitengrade der Unternehmen. |
| mapper.py | Darstellung der Unternehmen auf Heatmaps und Markerkarten. |

## Abhängigkeiten

- folium
- geopy
- numpy
- pandas

# Analysis

Zweck: Erstellung von Statistiken und Diagrammen.

## Allgemein

Ressourcen: Kategorisierte Websites.

Skripte:

| Datei | Nutzen |
| ------ | ------ |
| data.py | Erstellt Statistiken über den Crawl- und Kategorisierungsprozess. |
| plotter.py | Erstellt aus den Statistiken Diagramme. |

## Abhängigkeiten

- matplotlib
- numpy
- pandas

# Referenzen

<a name="myfootnote1">1</a>: Dirk Goldhahn, Thomas Eckart and Uwe Quasthoff (2012): Building Large Monolingual Dictionaries at the Leipzig Corpora Collection: From 100 to 200 Languages. In: Proceedings of the Eighth International Conference on Language Resources and Evaluation (LREC'12), 2012 ([URL](http://www.lrec-conf.org/proceedings/lrec2012/pdf/327_Paper.pdf)).