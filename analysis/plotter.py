import json
import matplotlib.pylab as plt
import numpy as np
import pandas as pd

from data import category_evaluation
from database.read import Connection


def get_data():

    connection = Connection()
    companies = connection.get_entries('companies', fields=['start_url', 'duns_id', 'hauptbranche_08_letter'])

    meta_relevant = []
    for company in companies:
        meta = connection.get_websites(company['start_url'])[0]
        if meta['hits'] > 0 and meta not in meta_relevant:
            meta_relevant.append(meta)

    return companies, meta_relevant


def boxplot_wz(level):

    if level not in ['hits', 'time_taken', 'chars']:
        level = 'hits'

    companies, meta = get_data()

    chars_relevant = []
    if level == 'chars':
        with open('chars.json', 'r') as file:
            chars = json.load(file)

        for element in meta:
            if element['start_url'] in chars:
                chars_relevant.append({'start_url': element['start_url'], 'chars': chars[element['start_url']]})

        meta = chars_relevant

    start_urls_meta = [entry['start_url'] for entry in meta]
    if level == 'chars':
        level_meta = [entry[level] * 1e-4 for entry in meta]
    else:
        level_meta = [entry[level] for entry in meta]
    start_urls_companies = [entry['start_url'] for entry in companies]
    hauptbranche_08_letters = [entry['hauptbranche_08_letter'] for entry in companies]

    df_meta = pd.DataFrame({'start_url': start_urls_meta, level: level_meta})
    df_companies = pd.DataFrame({'start_url': start_urls_companies, 'hauptbranche_08_letter': hauptbranche_08_letters})

    df_merged = pd.merge(df_meta, df_companies, on='start_url', how='left')

    labels = ['Ø', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S']

    dataset = []
    for label in labels:
        if label == 'Ø':
            dataset.append(df_meta[level])
        else:
            dataset.append(df_merged[df_merged['hauptbranche_08_letter'] == label][level])

    fig, ax = plt.subplots(figsize=(12, 7))

    if level == 'chars':
        ax.ticklabel_format(useOffset=False, style='plain', axis='y', )
        ax.text(0, 1, s=r'$\mathregular{10^{3}}$', transform=ax.transAxes,
                horizontalalignment='left', verticalalignment='top')

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)

    ax.set_xlabel('Hauptbranche 08')

    if level == 'hits':
        ax.set_ylabel('Anzahl Webseiten')
    elif level == 'time_taken':
        ax.set_ylabel('Ø Zeit in Sekunden')
    else:
        ax.set_ylabel('Ø Anzahl Zeichen')

    ax.grid(color='black', axis='y', linestyle='-', linewidth=0.25, alpha=0.5)
    ax.set_axisbelow(True)

    ax.boxplot(dataset, labels=labels, showfliers=False)
    plt.savefig(f'plots/boxplot_{level}_wz.png', bbox_inches='tight', pad_inches=0.015, dpi=300)


def histogram_hits():

    companies, meta = get_data()

    data = [entry['hits'] for entry in meta]

    counter = 0
    for dat in data:
        if dat <= 25:
            counter += 1
    print(counter / len(data))

    bins = [1]
    bins.extend(list(range(25, 251, 25)))
    fig, ax = plt.subplots(figsize=(12, 7))

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)

    ax.set_xticks(bins)

    ax.set_xlabel('Anzahl Webseiten')
    ax.set_ylabel('Anzahl Unternehmen')

    ax.grid(color='black', axis='y', linestyle='-', linewidth=0.5, alpha=0.5)
    ax.set_axisbelow(True)

    ax.hist(np.array(data), bins=bins, color='#2B4574')
    plt.savefig('plots/histogram_hits.png', bbox_inches='tight', pad_inches=0.015, dpi=300)


def histogram_hits_wz_average():

    companies, meta = get_data()

    start_urls_meta = [entry['start_url'] for entry in meta]
    hits_meta = [entry['hits'] for entry in meta]

    start_urls_companies = [entry['start_url'] for entry in companies]
    hauptbranche_08_letters = [entry['hauptbranche_08_letter'] for entry in companies]

    df_meta = pd.DataFrame({'start_url': start_urls_meta, 'hits': hits_meta})
    df_companies = pd.DataFrame({'start_url': start_urls_companies, 'hauptbranche_08_letter': hauptbranche_08_letters})

    df_merged = pd.merge(df_meta, df_companies, on='start_url', how='left')

    labels = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S']

    dataset = {label: 0 for label in labels}
    for label in labels:
        dataset[label] = df_merged[df_merged['hauptbranche_08_letter'] == label]['hits'].sum() / \
                         len(df_merged[df_merged['hauptbranche_08_letter'] == label])

    fig, ax = plt.subplots(figsize=(12, 7))

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)

    ax.set_xlabel('Hauptbranche')
    ax.set_ylabel('Ø Anzahl Webseiten')

    ax.grid(color='black', axis='y', linestyle='-', linewidth=0.5, alpha=0.5)
    ax.set_axisbelow(True)

    ax.bar(dataset.keys(), dataset.values(), color='#2B4574')
    plt.savefig('plots/histogram_hits_wz.png', bbox_inches='tight', pad_inches=0.015, dpi=300)


def histogram_result(category):

    data_c = category_evaluation('classification', category)
    data_r = category_evaluation('regression', category)

    average_c = {}
    for key, value in data_c.items():
        all_together = []
        for val in value.values():
            for v in val:
                if v == 1:
                    all_together.append(0.125)
                elif v == 2:
                    all_together.append(0.375)
                elif v == 3:
                    all_together.append(0.625)
                else:
                    all_together.append(0.875)
        average_c[key] = sum(all_together) / len(all_together)

    average_r = {}
    for key, value in data_r.items():
        all_together = []
        for val in value.values():
            all_together.extend(val)
        average_r[key] = sum(all_together) / len(all_together)

    print(average_c)
    print(average_r)

    fig, ax = plt.subplots(figsize=(12, 7))

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)

    x = np.arange(len(average_c.keys()))
    ticks = []

    if category == 'location':
        for key in average_c.keys():
            if key == 'Chemnitz, Stadt':
                key = 'Chemnitz,\nStadt'
            elif key == 'Dresden, Stadt':
                key = 'Dresden,\nStadt'
            elif key == 'Leipzig, Stadt':
                key = 'Leipzig,\nStadt'
            elif key == 'Mittelsachsen':
                key = 'Mittel-\nsachsen'
            elif key == 'Nordsachsen':
                key = 'Nord-\nsachsen'
            elif key == 'Vogtlandkreis':
                key = 'Vogtland-\nkreis'
            elif key == 'Erzgebirgskreis':
                key = 'Erzgebirgs-\nkreis'
            elif key == 'Sächsische Schweiz-Osterzgebirge':
                key = 'Sächsische\nSchweiz-\nOsterzgebirge'
            ticks.append(key)

    elif category == 'year':
        for key in average_c.keys():
            if key == 2001:
                key = 'bis 2000'
            elif key == 2006:
                key = '2001 bis 2005'
            elif key == 2011:
                key = '2006 bis 2010'
            elif key == 2016:
                key = '2011 bis 2015'
            else:
                key = 'ab 2016'
            ticks.append(key)
    else:
        for key in average_c.keys():
            ticks.append(key)

    plt.xticks(x, ticks)

    if category == 'size':
        ax.set_xlabel('Größenklasse')
    elif category == 'hauptbranche_08_letter':
        ax.set_xlabel('Branche')
    elif category == 'year':
        ax.set_xlabel('Gründungsjahr')
    else:
        ax.set_xlabel('Landkreis/kreisfreie Stadt')

    plt.yticks(fontsize=7.9)
    plt.xticks(fontsize=7.9)
    plt.ylim(0, 0.65)

    ax.set_ylabel('Ø Digitalisierungsgrad')

    ax.grid(color='black', axis='y', linestyle='-', linewidth=0.5, alpha=0.5)
    ax.set_axisbelow(True)

    w = 0.35
    ax.bar(x - w / 2, average_c.values(), w, color='#2B4574', label='Klassifikation')
    ax.bar(x + w / 2, average_r.values(), w, color='#F7904F', label='Regression')

    plt.legend(loc='upper left', prop={'size': 12}, bbox_to_anchor=[0, 0.91])

    plt.savefig(f'plots/result_{category}.png', bbox_inches='tight', pad_inches=0.015, dpi=300)


if __name__ == "__main__":

    boxplot_wz('chars')
    boxplot_wz('hits')
    boxplot_wz('time_taken')
    histogram_hits()
    histogram_hits_wz_average()

    histogram_result('size')
    histogram_result('hauptbranche_08_letter')
    histogram_result('location')
    histogram_result('year')
