import os
import re
import statistics

from database.read import Connection


def general_information():

    connection = Connection()
    companies = connection.get_companies()

    meta_relevant, same_websites = [], 0
    for company in companies:
        meta = connection.get_websites(company['start_url'])[0]
        if meta not in meta_relevant:
            meta_relevant.append(meta)
        else:
            same_websites += 1

    special = {'file': 0, 'duplicate': 0, 'empty': 0, 'error': 0, 'others': 0}
    for entry in meta_relevant:
        if entry['hits'] > 0:
            websites = connection.get_webpages(entry['start_url'])
            for website in websites:
                if website['note']:
                    special[website['note']] += 1
                else:
                    special['others'] += 1

    hits, limit_reached, blocked = [], 0, 0
    for entry in meta_relevant:
        if entry['hits'] == 0:
            blocked += 1
        else:
            hits.append(entry['hits'])
            if entry['limit_reached']:
                limit_reached += 1

    print('############ Companies ############')
    print(f'Successful: {len(hits)}')
    print(f'Total: {len(companies)}')
    print(f'Not available: {(16788 - len(companies))}')
    print(f'Duplicate: {same_websites}\n')

    print('############ Websites ############')
    print(f'Robots blocked: {blocked}\n')

    print('############ Hits ############')
    print(f'Total: {sum(hits)}')
    print(f'File: {special["file"]}')
    print(f'Duplicate: {special["duplicate"]}')
    print(f'Empty: {special["empty"]}')
    print(f'Error: {special["error"]}')
    print(f'Others: {special["others"]}\n')

    print(f'Mean: {statistics.mean(hits)}')
    print(f'Median: {statistics.median(hits)}')
    print(f'Limit reached: {limit_reached}')


def websites_size():

    sizes = []
    directory = 'E:/raw'
    for folder in os.listdir(directory):
        sizes.append(os.path.getsize(f'{directory}/{folder}/website_{folder}.html'))  # in Bytes

    print('############ Size ############')
    print(f'Total: {sum(sizes)}')
    print(f'Mean: {statistics.mean(sizes)}')
    print(f'Median: {statistics.median(sizes)}')


def websites_time():

    connection = Connection()
    companies = connection.get_companies()

    time_websites = []
    for company in companies:

        meta = connection.get_websites(company['start_url'])[0]
        if meta['hits'] > 0:
            time_websites.append(meta['time_taken'])

    print('############ Time ############')
    print(f'Total: {sum(time_websites)}')
    print(f'Mean: {statistics.mean(time_websites)}')
    print(f'Median: {statistics.median(time_websites)}')


def websites_excluded():

    connection = Connection()
    companies = connection.get_companies()

    all_urls = []
    excluded = []
    for company in companies:
        meta = connection.get_websites(company['start_url'])[0]
        if meta['hits'] > 0:
            all_urls.append(meta['start_url'])
            if company['hauptbranche_08_letter'] not in ['O', 'P', 'Q']:
                excluded.append(meta['start_url'])

    print('############ Excluded ############')
    print(f'Unique websites all: {len(list(dict.fromkeys(all_urls)))}')
    print(f'Companies all: {len(all_urls)}\n')
    print(f'Unique websites included: {len(list(dict.fromkeys(excluded)))}')
    print(f'Companies included: {len(excluded)}')


def websites_classified():

    connection = Connection()
    companies = connection.get_companies()

    tracker = {size: {1: 0, 2: 0, 3: 0, 4: 0} for size in ['classification', 'regression']}
    for company in companies:
        if company['hauptbranche_08_letter'] not in ['O', 'P', 'Q']:
            meta = connection.get_websites(company['start_url'])[0]
            if meta['hits'] > 0:
                classified = connection.get_classified(company['start_url'])[0]

                if classified['classification'] == 0:
                    tracker['classification'][1] += 1
                elif classified['classification'] == 1:
                    tracker['classification'][2] += 1
                elif classified['classification'] == 2:
                    tracker['classification'][3] += 1
                elif classified['classification'] == 3:
                    tracker['classification'][4] += 1

                if classified['regression'] < 0.25:
                    tracker['regression'][1] += 1
                elif classified['regression'] < 0.5:
                    tracker['regression'][2] += 1
                elif classified['regression'] < 0.75:
                    tracker['regression'][3] += 1
                else:
                    tracker['regression'][4] += 1

    print(tracker)


def category_evaluation(method, category):

    if category == 'size':
        labels = ['Kleinstunternehmen', 'Kleine Unternehmen', 'Mittlere Unternehmen', 'Großunternehmen']
    elif category == 'hauptbranche_08_letter':
        labels = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'R', 'S']
    elif category == 'year':
        labels = [2001, 2006, 2011, 2016, 2022]
    else:
        category = 'location'
        labels = ['Bautzen', 'Chemnitz, Stadt', 'Dresden, Stadt', 'Erzgebirgskreis', 'Görlitz', 'Leipzig',
                  'Leipzig, Stadt', 'Meißen', 'Mittelsachsen', 'Nordsachsen', 'Sächsische Schweiz-Osterzgebirge',
                  'Vogtlandkreis', 'Zwickau']

    connection = Connection()
    companies = connection.get_companies()

    tracker = {label: {1: [], 2: [], 3: [], 4: []} for label in labels}

    for company in companies:
        if company['hauptbranche_08_letter'] not in ['O', 'P', 'Q']:
            meta = connection.get_websites(company['start_url'])[0]
            if meta['hits'] > 0:
                classified = connection.get_classified(company['start_url'])[0]

                if category == 'year':
                    for label in labels:
                        if company[category] < label:
                            company[category] = label
                            break

                if method == 'regression':
                    if classified['regression'] < 0.25:
                        tracker[company[category]][1].append(classified['regression'])
                    elif classified['regression'] < 0.5:
                        tracker[company[category]][2].append(classified['regression'])
                    elif classified['regression'] < 0.75:
                        tracker[company[category]][3].append(classified['regression'])
                    else:
                        tracker[company[category]][4].append(classified['regression'])
                else:
                    if classified['classification'] == 1:
                        tracker[company[category]][1].append(classified['classification'])
                    elif classified['classification'] == 2:
                        tracker[company[category]][2].append(classified['classification'])
                    elif classified['classification'] == 3:
                        tracker[company[category]][3].append(classified['classification'])
                    else:
                        tracker[company[category]][4].append(classified['classification'])

    return tracker
