import json
import os
import re

from database.read import Connection


def load_websites(level, size, ignore_wz_codes):

    directory = f'{os.path.dirname(os.getcwd())}/extractor/output'

    connector = Connection()

    websites = {}
    with open(f'{directory}/websites_extracted_{level}_{size}.jsonl', 'r') as file:
        for line in file:
            loaded = json.loads(line)

            # exclude companies based on wz_code
            companies = connector.get_entries('companies', {'start_url': loaded['start_url']})

            included = False
            for company in companies:
                if company['hauptbranche_08_letter'] not in ignore_wz_codes:
                    included = True

            if included:

                merged_keywords = []
                for category in loaded['keywords'].keys():
                    merged_keywords.extend(loaded['keywords'][category])

                websites[loaded['start_url']] = {'structure': loaded['structure'],
                                                 'urls': loaded['urls'],
                                                 'emails': loaded['emails'],
                                                 'languages': loaded['languages'],
                                                 'keywords': merged_keywords}

    return websites


def load_keywords(level, categories=None):

    if categories is None:
        categories = ['mobile_app', 'crm_system', 'digital_technologies',
                      'digital_distribution', 'digital_products', 'digital_working']

    directory = f'{os.path.dirname(os.getcwd())}/lists/keywords'

    keywords = []
    for category in categories:

        file_name = f'{directory}/{category}.txt'
        if os.path.isfile(file_name):
            with open(file_name, 'r', encoding='utf8') as f:
                for line in f:
                    line = re.sub(r'\n', '', line)

                    if level == 'cluster' and re.match(r'^#', line):
                        cluster = re.sub(r'^# ', '', line).lower()
                        if cluster not in keywords:
                            keywords.append(cluster)

                    elif line.count(',') > 0:
                        parts = line.split(',')

                        if level == 'id' and parts[0] not in keywords:
                            keywords.append(parts[0].lower())

                        elif level == 'word' and parts[1] not in keywords:
                            keywords.append(parts[1].lower())

        else:
            print(f'Files with {category} keywords not found!')

    return keywords


def load_training_data(method, step, level):

    if method not in ['classification', 'regression']:
        method = 'classification'

    with open(f'training/data_{method}.jsonl', 'r', encoding='utf8') as file:
        for line in file:
            loaded = json.loads(line)
            if loaded['step'] == step and loaded['level'] == level:
                return loaded['vectors'], loaded['classes']

    print(f'{step}, {level}, {method}')
