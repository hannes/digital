import json
import numpy as np
import os

from classifier import Classificator, Regressor
from load import load_training_data


def evaluate_features(importance, level):

    tracker = {'website': 0, 'social_media': 0, 'international': 0, 'mobile_app': 0, 'crm_system': 0,
               'digital_technologies': 0, 'digital_distribution': 0, 'digital_products': 0, 'digital_working': 0}

    for index, weight in enumerate(np.nditer(importance)):

        if index < 5:
            tracker['website'] += weight
        elif index == 5:
            tracker['social_media'] += weight
        elif index == 6:
            tracker['crm_system'] += weight
        elif index < 9:
            tracker['international'] += weight
        elif (level == 'word' and index < 16) or (level == 'id' and index < 14) or (level == 'cluster' and index < 12):
            tracker['mobile_app'] += weight
        elif (level == 'word' and index < 42) or (level == 'id' and index < 24) or (level == 'cluster' and index < 19):
            tracker['crm_system'] += weight
        elif (level == 'word' and index < 184) or (level == 'id' and index < 117) or \
                (level == 'cluster' and index < 40):
            tracker['digital_technologies'] += weight
        elif (level == 'word' and index < 221) or (level == 'id' and index < 145) or \
                (level == 'cluster' and index < 47):
            tracker['digital_distribution'] += weight
        elif (level == 'word' and index < 245) or (level == 'id' and index < 157) or \
                (level == 'cluster' and index < 57):
            tracker['digital_products'] += weight
        else:
            tracker['digital_working'] += weight

    return tracker


def evaluate_full(directory='output'):

    path = f'{directory}/evaluation.jsonl'
    if os.path.exists(path):
        os.remove(path)

    if not os.path.exists(directory):
        os.makedirs(directory)

    methods = ['classification', 'regression']
    levels = ['word', 'id', 'cluster']
    steps = [25, 50, 100, 200, 250]
    c_values = [0.01, 0.03, 0.07, 0.1, 0.3, 0.7, 1, 3, 7, 10]

    for method in methods:

        for step in steps:
            for level in levels:

                x, y = load_training_data(method, step, level)

                for c_value in c_values:

                    print(f'Evaluate: {method}, {step}, {level}, {c_value}.')

                    if method == 'classification':
                        classificator = Classificator(x, y)
                        score = classificator.cross_validate(c_value)
                        feature_weight = evaluate_features(classificator.feature_importances(c_value), level)
                        entry = {'method': method, 'step': step, 'level': level, 'c_value': c_value,
                                 'score': {'macro_f1': score['test_f1_macro'].mean(),
                                           'micro_f1': score['test_f1_micro'].mean()},
                                 'feature_weight': feature_weight}

                    else:

                        scores, feature_weights = [], []
                        for vector, value in zip(x, y):
                            regressor = Regressor(vector, value)
                            score = regressor.cross_validate(c_value)
                            scores.append(-1 * score['test_score'].mean())
                            feature_weight = evaluate_features(regressor.feature_importances(c_value), level)
                            feature_weights.append(feature_weight)

                        score = sum(scores) / len(scores)
                        feature_weight = {key: 0 for key in feature_weights[0].keys()}
                        for key in feature_weight:
                            for weight in feature_weights:
                                feature_weight[key] += weight[key]

                        feature_weight = {key: value / len(feature_weights) for key, value in feature_weight.items()}

                        entry = {'method': method, 'step': step, 'level': level, 'c_value': c_value,
                                 'score': {'mse': score}, 'feature_weight': feature_weight}

                    with open(path, 'a', encoding='utf8') as file:
                        json.dump(entry, file)
                        file.write('\n')


if __name__ == "__main__":
    evaluate_full()
