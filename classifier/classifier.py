import numpy as np
from sklearn.model_selection import cross_validate
from sklearn.svm import SVC, SVR

from database.write import list_to_database
from load import load_keywords, load_websites, load_training_data


class Regressor:

    def __init__(self, x, y):

        self.x = x
        self.y = y

    def regression(self, websites, keywords):

        model = self.__train()

        regressed = {}
        for key, value in websites.items():
            vector = np.array(vectorize(value, keywords)).reshape(1, -1)
            regressed[key] = float(model.predict(vector)[0])

        return regressed

    def __train(self, c=0.1):

        model = SVR(kernel='linear', C=c)
        model.fit(self.x, self.y)

        return model

    # cross validate regression using mean squared error
    def cross_validate(self, c):

        model = SVR(kernel='linear', C=c)
        score = cross_validate(model, self.x, self.y, cv=10, scoring='neg_mean_squared_error')

        return score

    def feature_importances(self, c):

        coefs = self.__train(c).coef_
        coefs = np.sum(np.absolute(coefs), axis=0)

        return coefs


class Classificator:

    def __init__(self, x, y):

        self.x = x
        self.y = y

    def classification(self, websites, keywords):

        model = self.__train()

        classified = {}
        for key, value in websites.items():
            vector = np.array(vectorize(value, keywords)).reshape(1, -1)
            classified[key] = int(model.predict(vector)[0])

        return classified

    def __train(self, c=1):

        model = SVC(kernel='linear', C=c, decision_function_shape='ovo', class_weight='balanced')
        model.fit(self.x, self.y)

        return model

    # cross validate classification using micro and macro F1
    def cross_validate(self, c):

        model = SVC(kernel='linear', C=c, decision_function_shape='ovo', class_weight='balanced')
        scores = cross_validate(model, self.x, self.y, cv=10, scoring=('f1_macro', 'f1_micro'))

        return scores

    def feature_importances(self, c):

        coefs = self.__train(c).coef_
        coefs = np.sum(np.absolute(coefs), axis=0) / len(coefs)

        return coefs


def vectorize(element, keywords):

    vector = []
    vector.extend(element['structure'].values())

    if element['urls']['social_media']:
        vector.append(1)
    else:
        vector.append(0)

    if element['emails']:
        vector.append(1)
    else:
        vector.append(0)

    vector.append(element['languages']['en'])
    vector.append(element['languages']['others'])

    for keyword in keywords:
        if keyword in element['keywords']:
            vector.append(1)
        else:
            vector.append(0)

    return np.array(vector)


def predict(ignore_wz_codes):

    # classification, values based on evaluation
    method, step, level = 'classification', 250, 'cluster'
    websites = load_websites(level, step, ignore_wz_codes)
    keywords = load_keywords(level)
    x, y = load_training_data(method, step, level)
    classifier = Classificator(x, y)
    classified = classifier.classification(websites, keywords)

    # regression, values based on evaluation
    method, step, level, = 'regression', 200, 'id'
    websites = load_websites(level, step, ignore_wz_codes)
    keywords = load_keywords(level)
    x, y = load_training_data(method, step, level)

    # regression is based on 100 training sets, calculate average
    regressed = []
    for vector, value in zip(x, y):
        regressor = Regressor(vector, value)
        regressed.append(regressor.regression(websites, keywords))

    average = {key: 0 for key in regressed[0].keys()}
    for key in average:
        for company in regressed:
            average[key] += company[key]

    regressed = {key: value / len(x) for key, value in average.items()}

    # format output
    predicted_values = []
    for key in classified.keys():
        entry = {'start_url': key, 'classification': classified[key], 'regression': regressed[key]}
        predicted_values.append(entry)

    # write to database
    list_to_database(predicted_values, 'classified')


if __name__ == "__main__":

    excluded_codes = ['O', 'P', 'Q', 'unknown']  # excluded because they don't fit the classification scheme
    predict(excluded_codes)
