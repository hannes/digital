from pymongo import MongoClient


class Connection:

    def __init__(self):
        self.client = MongoClient('localhost', 27017, unicode_decode_error_handler='ignore')

    def get_entries(self, collection, condition=None, fields=None):

        if not condition:
            condition = {}

        if not fields:
            fields = []

        projection = {'_id': False}
        for field in fields:
            projection[field] = True

        cursor = self.client.test.get_collection(collection).find(condition, projection)
        return list(cursor)

    def get_companies(self):
        cursor = self.client.test.companies.find({}, {'_id': 0})
        return list(cursor)

    def get_companies_by_duns(self, duns_id):
        cursor = self.client.test.companies.find({'duns_id': duns_id}, {'_id': 0})
        return list(cursor)

    def get_websites(self, start_url):
        cursor = self.client.test.websites.find({'start_url': start_url}, {'_id': 0})
        return list(cursor)

    def get_webpages(self, start_url):
        cursor = self.client.test.webpages.find({'start_url': start_url}, {'_id': 0})
        return list(cursor)

    def get_webpages_by_url(self, url):
        cursor = self.client.test.webpages.find({'url': url}, {'_id': 0})
        return list(cursor)

    def get_classified(self, start_url):
        cursor = self.client.test.classified.find({'start_url': start_url}, {'_id': 0})
        return list(cursor)

    def get_geocodes(self):
        cursor = self.client.test.geocodes.find({}, {'_id': 0})
        return list(cursor)

    def get_geocodes_by_duns(self, duns_id):
        cursor = self.client.test.geocodes.find({'start_url': duns_id}, {'_id': 0})
        return list(cursor)
