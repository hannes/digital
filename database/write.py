import json
from pymongo import MongoClient
from pymongo.errors import DocumentTooLarge
import os.path

import pandas as pd


def get_database():

    client = MongoClient('localhost', 27017)
    return client.get_database('digitalmonitor')


def list_to_database(data, collection_name):

    collection = get_database().get_collection(collection_name)

    for entry in data:
        try:
            collection.insert_one(entry)
        except DocumentTooLarge:
            print(f'Document too large: {entry["url"]}')


def file_to_database(file, collection_name):

    collection = get_database().get_collection(collection_name)

    if os.path.isfile(file):
        with open(file, 'r') as file:
            for line in file:
                data = json.loads(line)

                try:
                    collection.insert_one(data)
                except DocumentTooLarge:
                    print(f'Document too large: {data["url"]}')


def companies_to_database(file, collection_name):

    collection = get_database().get_collection(collection_name)

    # Hauptbranche 08 is only expressed as a number (too fine-grained), transform to letter code for better analysis
    wz_codes = pd.read_csv('wz_codes.csv', converters={'Startnummer': lambda x: str(x)}, encoding='utf8')

    if os.path.isfile(file):
        with open(file, 'r') as file:
            for line in file:

                data = json.loads(line)
                if isinstance(data['hauptbranche_08'], str):
                    wz_code = data['hauptbranche_08']
                    code_letter = wz_codes.loc[wz_codes['Startnummer'] == wz_code[0:2]]['Schlüssel'].values[0]
                    data['hauptbranche_08_letter'] = code_letter
                else:
                    data['hauptbranche_08_letter'] = 'unknown'

                # add size of company based on number of employees and annual turnover
                turnover = data['turnover']
                if isinstance(turnover, str):
                    turnover = float(turnover.replace(',', '.'))
                if data['employees'] <= 9 and turnover <= 2:
                    size = 'Kleinstunternehmen'
                elif data['employees'] <= 49 and turnover <= 10:
                    size = 'Kleine Unternehmen'
                elif data['employees'] <= 249 and turnover <= 50:
                    size = 'Mittlere Unternehmen'
                else:
                    size = 'Großunternehmen'
                data['size'] = size

                try:
                    collection.insert_one(data)
                except DocumentTooLarge:
                    print(f'Document too large: {data["url"]}')


if __name__ == "__main__":

    directory = f'{os.path.dirname(os.getcwd())}/crawler/output'

    companies_to_database(f'{directory}/companies.jsonl', 'companies')
    file_to_database(f'{directory}/websites.jsonl', 'websites')
    file_to_database(f'{directory}/webpages.jsonl', 'webpages')
