class RobotsNotFoundException(Exception):
    pass


class InvalidRedirectedSchemeException(Exception):
    pass


class WebDriverInitiationException(Exception):
    pass
