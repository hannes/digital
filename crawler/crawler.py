import http.client
import logging
import re
from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.remote.command import Command
import socket
import time
from urllib.parse import urlsplit

from robots import Robots
from exceptions import InvalidRedirectedSchemeException, WebDriverInitiationException


class Crawler:

    def __init__(self, url, timeout=15, max_depth=0, limit=0):

        self.start_url = url
        self.timeout = timeout

        self.result = []
        self.time_taken = 0
        self.limit_reached = False

        # scheme, netloc and host
        self.scheme = urlsplit(self.start_url).scheme
        netloc = urlsplit(self.start_url).netloc
        netloc = re.sub(r':\d*$', '', netloc)  # remove port
        host = f'{self.scheme}://{netloc}'
        self.netloc = re.sub(r'^www\.', '', netloc)

        log = f' Timeout: {timeout}.'
        if max_depth >= 0:
            log = f'{log} Page depth maximum: {max_depth}.'
        if limit >= 0:
            log = f'{log} Limit: {limit}.'
        logging.info(f' Crawling {self.start_url}. Host: {host}.{log}')

        # read and set robots.txt
        self.robots = Robots(host, self.timeout)

        if self.robots.can_crawl(self.start_url):

            # init driver
            self.opts = Options()
            self.opts.add_argument('--headless')
            self.__init_new_driver()

            # init trackers for all found / already crawled URLs and their sources
            self.all_urls = [self.start_url]
            self.crawled_urls = []
            self.sources = {self.start_url: self.start_url}

            # set parameter
            self.max_depth = max_depth
            self.limit = limit

            # extensions excluded from requesting
            self.excluded_extensions = tuple(['.pdf', '.txt', '.doc', '.docx', '.xls', '.xlsx', '.ods', '.ppt', '.pptx',
                                              '.zip', '.gz', '.rar', '.7z',
                                              '.csv', '.log', '.sav', '.exe', '.jar',
                                              '.jpg', '.jpeg', '.png', '.gif', '.ico', '.bmp',
                                              '.mp3', '.mp4', '.webm', '.mpg', '.avi'])

            # crawl
            self.current_depth = 0

            start_time = time.time()
            self.result = self.__start_crawling()
            self.time_taken = time.time() - start_time

            logging.info(f' Crawled {len(self.result)} URLs in {self.time_taken} seconds.')

            self.driver.quit()

        else:
            logging.info(f' Not allowed to crawl {url}.')

    def __init_new_driver(self):

        for retry in range(3):
            try:
                self.driver = webdriver.Chrome(options=self.opts)
                self.driver.set_page_load_timeout(self.timeout)

                if retry != 0:
                    logging.info(f' Success!')

                return True

            except Exception as e:
                logging.error(f' {type(e).__name__} while initiating a new WebDriver. Retrying... ({retry + 1}/3).')
                time.sleep(2)

        logging.info(f' Failed!')
        raise WebDriverInitiationException

    def __check_driver(self):

        try:
            self.driver.execute(Command.STATUS)
        except (socket.error, http.client.CannotSendRequest):
            logging.info(f' Driver crashed! Initiating new driver...')
            self.__init_new_driver()

    def __open_url(self, url):

        self.__check_driver()

        try:
            self.driver.get(url)
        except Exception as e:
            self.driver.delete_all_cookies()
            raise e

    def __normalize(self, url):

        # ignore URLs with schemes like mailto or tel
        scheme_url = urlsplit(url).scheme
        if '@' in url or (scheme_url and not re.match(r'^http(s)?', scheme_url)):
            return None

        # remove incomplete queries/fragments and trailing slashes
        url = re.sub(r'[?#&]+$', '', url)
        url = re.sub(r'\/{2,}$', '/', url)

        # normalize scheme
        if re.match(r'^http(s?)', url):
            url = re.sub(r'^http(s?)', self.scheme, url)
        else:
            url = f'{self.scheme}://{url}'

        return url

    # check if allowed to crawl / has same host or subdomain
    def __is_relevant(self, url):

        if not self.robots.can_crawl(url):
            return False

        netloc_found = urlsplit(url).netloc

        # remove www and ports
        netloc_found = re.sub(r'^www\.', '', netloc_found)
        netloc_found = re.sub(r':\d*$', '', netloc_found)

        if netloc_found == self.netloc or netloc_found.endswith(f'.{self.netloc}'):
            return True
        else:
            return False

    # check if similar URL was already found
    def __similarity(self, url, secondary_reference=None):

        if not secondary_reference:
            secondary_reference = []

        if re.match(r'\/+$', url):
            modified = re.sub(r'\/+$', '', url)
        else:
            modified = f'{url}/'

        modified_lower = modified.lower()

        if modified in self.all_urls or modified in secondary_reference:
            return modified

        elif modified_lower in self.all_urls or modified_lower in secondary_reference:
            return modified_lower

        else:
            return url

    def __start_crawling(self):

        result = []
        found_urls = [self.start_url]

        while found_urls and (self.max_depth < 0 or self.current_depth <= self.max_depth):

            if self.limit >= 0 and len(found_urls) > self.limit - len(result):
                found_urls = found_urls[:self.limit - len(result)]
                self.limit_reached = True

            crawled = self.__crawl(found_urls)

            if crawled:

                result.extend(crawled)

                # retrieve found internal URLs
                found_urls = []
                for item in crawled:
                    found_urls.extend(item['internal_urls'])

                found_urls = list(dict.fromkeys(found_urls))  # remove duplicates
                found_urls = [found for found in found_urls if found not in self.all_urls]  # extract new URLs
                found_urls.sort(key=len)  # prioritize short URLs

                self.all_urls.extend(found_urls)

                self.current_depth += 1

            else:
                break

        return result

    def __crawl(self, urls):

        result = []

        for url in urls:

            new_entry = {'url': url, 'redirects_to': url, 'source': self.sources[url], 'depth': self.current_depth,
                         'timestamp': time.time(), 'html': '', 'internal_urls': [], 'note': ''}

            self.crawled_urls.append(url)

            if url.lower().endswith(self.excluded_extensions):
                new_entry['note'] = 'file'
            else:
                try:
                    self.__open_url(url)
                    redirected_url = self.__normalize(self.driver.current_url)

                    if not redirected_url:
                        raise InvalidRedirectedSchemeException

                    elif url != redirected_url:
                        new_entry['redirects_to'] = redirected_url
                        if redirected_url in self.crawled_urls:
                            new_entry['note'] = 'duplicate'
                            result.append(new_entry)
                            continue
                        else:
                            self.crawled_urls.append(redirected_url)

                    new_entry['html'] = self.driver.page_source

                    if not new_entry['html']:
                        new_entry['note'] = 'empty'

                    hrefs = self.driver.find_elements_by_xpath('//a[@href]')

                except WebDriverInitiationException:
                    raise WebDriverInitiationException

                except Exception as e:
                    logging.error(f' {type(e).__name__} while crawling {url}!')
                    new_entry['note'] = 'error'
                    result.append(new_entry)
                    continue

                # extract internal URLs
                internal_urls = []
                hrefs = list(dict.fromkeys(hrefs))

                if len(hrefs) <= 1000:  # ignore websites with more than 1000 URLs, most likely spam
                    for href in hrefs:
                        try:
                            found = self.__normalize(href.get_attribute('href'))
                            if found:
                                found = self.__similarity(found, internal_urls)  # avoid duplicates
                                if found not in internal_urls and self.__is_relevant(found):
                                    internal_urls.append(found)
                                    if found not in self.sources:
                                        self.sources[found] = url

                        except StaleElementReferenceException:
                            continue
                        except Exception as e:
                            logging.error(f' {type(e).__name__} while extracting internal URLs at {url}!')
                            new_entry['note'] = 'error'

                new_entry['internal_urls'] = internal_urls

            result.append(new_entry)

        return result
