import logging
import requests
import urllib.robotparser as urobot

from exceptions import RobotsNotFoundException


class Robots:
    def __init__(self, host, timeout):

        # read robots.txt, if available
        robots = f'{host}/robots.txt'

        try:
            r = requests.head(robots, timeout=timeout)

            if r.status_code not in [301, 302] and r.status_code < 400:
                self.rp = urobot.RobotFileParser()
                self.rp.set_url(robots)
                self.rp.read()
                logging.info(f' File robots.txt found and read.')
            else:
                raise RobotsNotFoundException

        except Exception as e:
            self.rp = None
            logging.error(f' {type(e).__name__} while accessing robots.txt!')

    def can_crawl(self, url):

        if not self.rp or self.rp.can_fetch('*', url):
            return True
        else:
            return False
