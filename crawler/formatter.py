import json
from langdetect import detect, LangDetectException
import os.path
import re
import time
from urllib.parse import urlsplit

from parsing import ParserWebsite


class Formatter:

    def __init__(self, result, duplicate=False):

        self.result = result
        self.duplicate = duplicate
        self.start_url = result['start_url']

        self.__format()

    def __format(self):

        self.companies, self.meta, self.websites = {}, {}, []

        if self.start_url:
            self.companies = {'company_name': self.result['company_name'], 'duns_id': self.result['duns_id'],
                              'year': self.result['year'], 'location': self.result['location'],
                              'hauptbranche_08': self.result['hauptbranche_08'], 'employees': self.result['employees'],
                              'turnover': self.result['turnover'], 'start_url': self.result['start_url']}

        if not self.duplicate:
            self.meta = {'start_url': self.start_url, 'parameter': self.result['parameter'],
                         'hits': self.result['hits'], 'limit_reached': self.result['limit_reached'],
                         'time_taken': self.result['time_taken'], 'timestamp': time.time()}

            self.websites = self.__parse()

    def __parse(self):

        host = urlsplit(self.start_url).netloc
        host = re.sub(r'^www\.', '', host)
        host = re.sub(r':\d*$', '', host)

        result = []
        for website in self.result['data']:

            website_clean = {'start_url': self.start_url, 'url': website['url'],
                             'redirects_to': website['redirects_to'], 'source': website['source'],
                             'depth': website['depth'], 'timestamp': website['timestamp'],
                             'language': '', 'note': '', 'sentences': [], 'external_urls': [], 'emails': []}

            if website['note']:
                website_clean['note'] = website['note']
            else:
                parser = ParserWebsite(website['html'], host)

                website_clean['external_urls'], website_clean['emails'] = parser.extract_hrefs()
                website_clean['sentences'] = parser.extract_sentences()

                try:
                    website_clean['language'] = detect(' '.join(website_clean['sentences']))
                except LangDetectException:
                    pass

            result.append(website_clean)

        return result

    def save_as_json(self, directory='output'):

        if not os.path.exists(directory):
            os.makedirs(directory)

        if self.companies:
            with open(f'{directory}/companies.jsonl', 'a') as file:
                json.dump(self.companies, file)
                file.write('\n')

        if self.meta:
            with open(f'{directory}/websites.jsonl', 'a') as file:
                json.dump(self.meta, file)
                file.write('\n')

        if self.websites:
            with open(f'{directory}/webpages.jsonl', 'a') as file:
                for website in self.websites:
                    json.dump(website, file)
                    file.write('\n')
