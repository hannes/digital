from lxml import html
from lxml.html.clean import Cleaner
import re
from trafilatura import extract
from urllib.parse import urlsplit


class ParserWebsite:

    def __init__(self, website, host):

        cleaner = Cleaner()

        # remove javascript and inline-style
        cleaner.javascript = True
        cleaner.style = True

        website = html.fromstring(website)
        self.tree = cleaner.clean_html(website)

        self.host = host

    def extract_content_of_tag(self, tag):

        result = []
        for element in self.tree.xpath(f'//{tag}'):
            text = re.sub(r'\n', ' ', element.text_content())
            text = re.sub(r'\s{2,}', ' ', text)
            text = text.strip()

            if text:
                result.append(text)

        return list(dict.fromkeys(result))

    def extract_attribute_of_tag(self, tag, attribute):

        result = []
        for element in self.tree.xpath(f'//{tag}/@{attribute}'):
            result.append(element)

        return list(dict.fromkeys(result))

    def extract_hrefs(self):

        hrefs = self.extract_attribute_of_tag('a', 'href')

        external_urls, emails = [], []
        for href in hrefs:

            href = href.lower()
            scheme_url = urlsplit(href).scheme

            if re.match(r'^.+@.+$', href) and re.match(r'^[^!?,=&]*$', href):

                email = re.sub(r'^[^:]*:\s?', '', href)
                if re.match(r'^[^\s]*$', email) and \
                        re.match(r'[a-z0-9._\-]+(?:@|\(at\))[a-z0-9._\-]+(?:\.|\(dot\))[a-z]{2,}', email):
                    emails.append(email)

            elif not scheme_url or re.match(r'^http(s)?', scheme_url):

                netloc = urlsplit(href).netloc
                netloc = re.sub(r'^www\.', '', netloc)
                netloc = re.sub(r':\d*$', '', netloc)

                if not netloc.endswith(self.host) and netloc.count('.') > 0 and netloc not in external_urls:
                    external_urls.append(netloc)

        return list(dict.fromkeys(external_urls)), list(dict.fromkeys(emails))

    def extract_main_content(self):

        try:
            result = extract(self.tree)
        except AssertionError:
            result = None

        return result

    def extract_sentences(self):
        return extract_sentences(self.extract_main_content())


def extract_sentences(text):

    result = []
    if text:
        cleaned = clean_text(text)
        cleaned_list = cleaned.split('\n')

        for element in cleaned_list:

            # limit maximum sentence length to 255 to avoid catastrophic backtracking
            pattern = r'[A-ZÄÖÜ][^.!?]{,255}(?:[.!?](?![\'"]?\s|$)[^.!?]{,255}){,255}[.!?](?=\s|$)'
            sentences = re.findall(pattern, element)

            for sentence in sentences:
                sentence = sentence.strip()
                if 15 <= len(sentence) <= 255 and sentence.count(' ') >= 3:
                    result.append(sentence)

    return list(dict.fromkeys(result))


def clean_text(text):

    # escape characters
    text = re.sub(r'\n{2,}', '\n', text)
    text = re.sub(r'[\t\b\f\r\v]', ' ', text)

    # coding errors
    text = re.sub(r'[]', ' ', text)
    text = re.sub(r'[ ​﻿ ‍­̣ 　⠀]', ' ', text)

    # Umlaute
    text = re.sub(r'ä|ã¤', 'ä', text)
    text = re.sub(r'Ä|Ã¤', 'Ä', text)
    text = re.sub(r'ö|ã¶', 'ö', text)
    text = re.sub(r'Ö|Ã¶', 'Ö', text)
    text = re.sub(r'ü|ã¼', 'ü', text)
    text = re.sub(r'Ü|Ã¼', 'Ü', text)
    text = re.sub(r'ß', 'ss', text)

    # special characters except @_:/ (e-mails and URLs)
    text = re.sub(r'[;⁄\\*^⊃=≠÷+±➕➖¬▪¤×~≈¿¡⋅∙·•͑ ̛°˚⁾←⮜◀◄≪«≤≦‹<>›≧≥»≫►▶⮞➤→↑↓⇐⇑⇓⇒➥]', ' ', text)
    text = re.sub(r'[€$£%&§#¶¦|†…̈™®©⁰¹²³⁴⁵⁶⁷⁸⁹ⁱ�✓√✔✅○□★⭐■►●♦♥☎☏✉♩♪♫♬ⓘ⚠☝☞▽️∆№]', ' ', text)

    # normalize
    text = re.sub(r'[\[{]', '(', text)
    text = re.sub(r'[\]}]', ')', text)
    text = re.sub(r'[‟¨˝„“”″]', '"', text)
    text = re.sub(r'[‛’`´′‘]', '\'', text)
    text = re.sub(r'[‐‑–—‒−]', '-', text)
    text = re.sub(r'‚', ',', text)

    # repetitions
    text = re.sub(r'\s{2,}', ' ', text)
    text = re.sub(r'"{2,}', '"', text)
    text = re.sub(r'\'{2,}', '\'', text)
    text = re.sub(r'-{2,}', '-', text)
    text = re.sub(r',{2,}', ',', text)
    text = re.sub(r'\.{2,}', '.', text)
    text = re.sub(r'\?{2,}', '?', text)
    text = re.sub(r'!{2,}', '!', text)

    # spaces
    text = re.sub(r'\s,', ',', text)
    text = re.sub(r'\s\.', '.', text)
    text = re.sub(r'\s\?', '?', text)
    text = re.sub(r'\s!', '!', text)

    return text
