import json
import logging.config
import os.path
import pandas as pd
import re
import requests
from requests.exceptions import SSLError
import sys

from crawler import Crawler
from exceptions import WebDriverInitiationException
from formatter import Formatter


def init():

    """
    Index | Col | Meaning
    0     | A   | Status (status)
    1     | B   | Firmenname (name of the company)
    2     | C   | D&B D-U-N-S®-Nummer
    3     | D   | Gründungsjahr (founding year)
    4     | E   | Landkreis/kreisfreie Stadt
    5     | F   | URLs; comma-separated String
    6     | G   | Hauptbranche 08
    7     | H   | Beschäftigtenzahl (number of employees)
    8     | I   | Jahresumsatz in Millionen € (annual turnover in millions)
    """

    # load input file (make sure the coluzms)
    companies = pd.read_excel('resources/data_companies.xls', usecols='A,B,C,D,E,F,G,H,I',
                              converters={'Hauptbranche 08': str})

    # drop companies without required data entries (D, E, G, H and I)
    companies = companies.dropna(subset=['GrÃ¼ndung (Numerisch)', 'Kreis', 'Hauptbranche 08',
                                         'BeschÃ¤ftigte', 'Umsatz'])

    parameter = {'timeout': 15, 'max_depth': -1, 'limit': 250}

    # init logging
    logging.config.fileConfig('config/logging.conf', disable_existing_loggers=True)

    # retrieve last crawled company (line), output file and URLs
    tracker = {'line': 0, 'urls': []}
    if os.path.isfile('save.data'):
        with open('save.data', 'r') as f:
            tracker = json.load(f)
            tracker['line'] += 1

    if tracker['line'] < len(companies):
        companies = companies.values[tracker['line']:].tolist()
        crawl(companies, parameter, tracker)


def crawl(companies, parameter, tracker):

    counter = 0
    for company in companies:

        if company[0] == 'aktiv':

            url = list(dict.fromkeys(company[5].split(', ')))[0]  # companies can have more than one website
            backup = url
            url = validate_url(url, parameter['timeout'])

            if url:
                result = {'company_name': company[1], 'duns_id': company[2], 'year': company[3], 'location': company[4],
                          'hauptbranche_08': company[6], 'employees': company[7], 'turnover': company[8],
                          'start_url': url, 'parameter': parameter, 'hits': 0, 'limit_reached': False,
                          'time_taken': 0, 'data': []}

                if url not in tracker['urls']:

                    duplicate = False

                    try:
                        crawler = Crawler(url, parameter['timeout'], parameter['max_depth'], parameter['limit'])

                        result['data'] = crawler.result
                        result['hits'] = len(crawler.result)
                        result['time_taken'] = crawler.time_taken
                        result['limit_reached'] = crawler.limit_reached

                    except WebDriverInitiationException:
                        logging.critical(f' Company ID: {company[2]}. Line: {tracker["line"]}.'
                                         f' WebDriverInitiationException while crawling {backup}.'
                                         f' Please validate or update your driver!')
                        sys.exit()

                else:
                    duplicate = True
                    logging.info(f' Duplicate URL: {backup} was already crawled!')

                # save result, if no error occured on first webpage
                if not (result['hits'] == 1 and result['data'][0]['note'] == 'error'):
                    formatter = Formatter(result, duplicate)
                    formatter.save_as_json()

                tracker['urls'].append(url)

        with open('save.data', 'w') as file:
            json.dump(tracker, file)

        counter += 1
        tracker['line'] += 1


def validate_url(url, timeout=15):

    # temporarily use HTTP
    if not re.match(r'^http(s?):', url):
        url = f'http://{url}'

    # request URL and get redirected URL
    try:
        request = requests.get(url, timeout=timeout)
        if request.status_code >= 400:
            logging.error(f' Client or Server error ({request.status_code})!')
            return None
        url = request.url
    except Exception as e:
        logging.error(f' {type(e).__name__} while requesting {url}!')

        try:
            requests.head('https://www.google.com/', timeout=timeout)
        except requests.ConnectionError:
            logging.critical(f' No internet connection detected! Terminating process...')
            sys.exit()

        return None

    # validate HTTPS protocol
    if re.match(r'^https:', url):
        try:
            requests.head(url, timeout=timeout)
        except SSLError:
            url = re.sub(r'^https:', 'http:', url)
            logging.error(f' SSLError while validating {url}! Protocol was changed to HTTP.')
        except Exception as e:
            logging.error(f' {type(e).__name__} while validating {url}!')
            return None

    return url


if __name__ == "__main__":
    init()
