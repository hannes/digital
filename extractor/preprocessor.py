from nltk import word_tokenize
import os
import re


class Preprocessor:

    def __init__(self):

        self.stopwords = {'de': load_stopwords('stopwords_de.txt'), 'en': load_stopwords('stopwords_en.txt')}
        self.baseforms = load_baseforms('baseforms.txt')

    def preprocess_text(self, text_lowercase, language='en'):

        tokens = word_tokenize(clean(text_lowercase))

        # lemmatization with POS-tags and stop-word removal
        cleaned_tokens = []
        for token in tokens:

            if token in self.baseforms:
                cleaned_tokens.append(self.baseforms[token])

            elif token not in self.stopwords[language]:
                cleaned_tokens.append(token)

        return cleaned_tokens


def load_stopwords(file):

    directory = f'{os.path.dirname(os.getcwd())}/lists'
    file_name = f'{directory}/stopwords/{file}'

    stopwords = []
    if os.path.isfile(file_name):
        with open(file_name, 'r', encoding='utf8') as f:
            for line in f:
                line = re.sub(r'\n', '', line)
                stopwords.append(line.lower())

    return stopwords


def load_baseforms(file):

    directory = f'{os.path.dirname(os.getcwd())}/lists'
    file_name = f'{directory}/baseforms/{file}'

    baseforms = {}
    if os.path.isfile(file_name):
        with open(file_name, 'r', encoding='utf8') as f:
            for line in f:
                line = re.sub(r'\n', '', line)
                parts = line.split(',')
                if len(parts) == 2:
                    baseforms[parts[0].lower()] = parts[1].lower()

    return baseforms


def clean(text):

    text = re.sub(r'ä', 'ae', text)
    text = re.sub(r'ö', 'oe', text)
    text = re.sub(r'ü', 'ue', text)
    text = re.sub(r'ß', 'ss', text)

    text = re.sub(r'[a-z0-9._\-]+(?:@|\(at\))[a-z0-9._\-]+(?:\.|\(dot\))[a-z]{2,}', '<email>', text)
    text = re.sub(r'(?:http[s]?:\/\/|www\.)(?:[a-z0-9._\-]+\.[a-z]{2,})+(?:\/[a-z0-9\-_.!?#]*)*', '<url>', text)

    text = re.sub(r'["\'\-_:@/]', ' ', text)
    text = re.sub(r'\s{2,}', ' ', text)

    return text
