import copy
import logging
from nltk import ngrams
from rapidfuzz.process import extractOne
from rapidfuzz.string_metric import normalized_levenshtein
import re

from preprocessor import Preprocessor

preprocessor = Preprocessor()


class FeatureExtractor:

    def __init__(self, website, webpages, domains, keywords, steps=None):

        extracted = []

        if steps:
            steps.append(len(webpages))
        else:
            steps = [len(webpages)]

        self.result_parts = []  # for analysis purpose, can be removed or ignored in future iterations
        self.result = {'start_url': website['start_url'],
                       'structure': {'size': website['hits'],
                                     'file': 0,
                                     'duplicate': 0,
                                     'empty': 0,
                                     'error': 0},
                       'urls': {'social_media': [],
                                'others': []},
                       'emails': [],
                       'languages': {},
                       'keywords': {'mobile_app': [],
                                    'crm_system': [],
                                    'digital_technologies': [],
                                    'digital_products': [],
                                    'digital_distribution': [],
                                    'digital_working': []}
                       }

        step = 1
        for webpage in webpages:

            # emails from URLs, already extracted in a previous step
            self.result['emails'].extend(email for email in webpage['emails'] if email not in self.result['emails'])

            # categorize URLs
            extracted_urls = extract_urls(webpage['external_urls'], domains)
            for key, value in extracted_urls.items():
                self.result['urls'][key].extend(url for url in value if url not in self.result['urls'][key])

            if webpage['note']:
                self.result['structure'][webpage['note']] += 1
            elif not webpage['sentences']:
                self.result['structure']['empty'] += 1
            elif webpage['sentences'] in extracted:
                self.result['structure']['duplicate'] += 1
            else:

                # track languages
                language = webpage['language']
                if language in self.result['languages']:
                    self.result['languages'][language] += 1
                elif language:
                    self.result['languages'][language] = 1

                text = ' '.join(webpage['sentences']).lower()

                # extract emails from text
                emails = re.findall(r'[a-z0-9._\-]+(?:@|\(at\))[a-z0-9._\-]+(?:\.|\(dot\))[a-z]{2,}', text)
                self.result['emails'].extend(email for email in emails if email not in self.result['emails'])

                # exclude text of data protection websites since they aren't representative
                if len(re.findall(r'datenschutz', text)) + len(re.findall(r'data protection', text)) <= 3:

                    # extract keywords from text if language is supported
                    if language in ['de', 'en']:

                        tokens = preprocessor.preprocess_text(text, language)

                        extracted_keywords = extract_keywords(tokens, language, keywords)
                        for keyword in extracted_keywords:
                            category = keywords[keyword]['category']
                            if keyword not in self.result['keywords'][category]:
                                self.result['keywords'][category].append(keyword)

                else:
                    logging.info(f' Ignoring data protection website: {webpage["url"]}.')

                extracted.append(webpage['sentences'])

            if step in steps:
                self.__save(step)

            step += 1

    def __save(self, step):

        copied = copy.deepcopy(self.result)
        copied['structure']['size'] = step
        self.result_parts.append(copied)


def extract_urls(urls, domains):

    found_urls = {'social_media': [], 'others': []}
    for url in urls:

        # extract domain from URL
        domain = re.sub(r'\.[^.]*$', '', url)
        if domain.count('.') > 0:
            domain = re.sub(r'^.*\.', '', domain)

        if domain in domains:
            if domain not in found_urls['social_media']:
                found_urls['social_media'].append(domain)
        elif url not in found_urls['others']:
            found_urls['others'].append(url)

    return found_urls


def extract_keywords(tokens, language, keywords, score_cutoff=85):

    found_keywords = []
    for keyword, meta in keywords.items():
        if meta['language'] == 'en' or language == meta['language']:

            n_grams = [' '.join(grams) for grams in ngrams(tokens, len(meta['cleaned'].split()))]
            if extractOne(meta['cleaned'], n_grams, scorer=normalized_levenshtein, score_cutoff=score_cutoff):
                found_keywords.append(keyword)

    return found_keywords
