import os.path
import re

from preprocessor import Preprocessor


# load keywords with meta-data
def load_keywords(categories):

    directory = f'{os.path.dirname(os.getcwd())}/lists/keywords'

    preprocessor = Preprocessor()

    keywords = {}
    for category in categories:

        cluster = 'undefined'

        file_name = f'{directory}/{category}.txt'
        if os.path.isfile(file_name):
            with open(file_name, 'r', encoding='utf8') as f:
                for line in f:
                    line = re.sub(r'\n', '', line)

                    if re.match(r'^#', line):
                        cluster = re.sub(r'^# ', '', line).lower()

                    elif line.count(',') > 1:
                        parts = line.split(',')
                        word_id, word, language = parts[0].lower(), parts[1].lower(), parts[2].lower()
                        keywords[word] = {'word': word, 'id': word_id, 'cluster': cluster,
                                          'category': category, 'language': language,
                                          'cleaned': ' '.join(preprocessor.preprocess_text(word))}
        else:
            print(f'File with keywords for category {category} not found.')

    sorted_keywords = {}
    for keyword in sorted(keywords.keys(), key=len, reverse=True):
        sorted_keywords[keyword] = keywords[keyword]

    return sorted_keywords
