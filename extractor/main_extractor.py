import copy
import json
import logging.config
import os.path

from load_keywords import load_keywords
from modify_output import modify_output
from database.read import Connection
from extractor import FeatureExtractor


def init():

    # init logging
    logging.config.fileConfig('config/logging.conf', disable_existing_loggers=True)

    # retrieve list of already extracted websites
    tracker = {'urls': []}
    if os.path.isfile('save.data'):
        with open('save.data', 'r') as f:
            tracker = json.load(f)

    steps = [25, 50, 100, 200, 250]  # for analysis purpose, can be removed or ignored in future iterations

    extract(tracker, steps)
    modify_output(steps)  # split output file


def extract(tracker, steps):

    connector = Connection()
    companies = connector.get_companies()

    domains = load_keywords(['social_media'])
    keywords = load_keywords(['mobile_app', 'crm_system', 'digital_technologies',
                              'digital_products', 'digital_distribution', 'digital_working'])

    for company in companies:

        start_url = company['start_url']

        if start_url not in tracker['urls']:

            website = connector.get_websites(start_url)[0]
            webpages = connector.get_webpages(start_url)

            if website['hits'] > 0:

                logging.info(f'Extracting features and keywords for company {website["start_url"]}.')
                extracted = FeatureExtractor(website, webpages, domains, keywords,  copy.deepcopy(steps))

                if steps:
                    save(extracted.result_parts)
                else:
                    save(extracted.result)

                tracker['urls'].append(start_url)
                with open('save.data', 'w') as file:
                    json.dump(tracker, file)


def save(result, directory='output'):

    if not os.path.exists(directory):
        os.makedirs(directory)

    with open(f'{directory}/websites_extracted.jsonl', 'a') as file:
        if isinstance(result, list):
            for result_part in result:
                json.dump(result_part, file)
                file.write('\n')
        else:
            json.dump(result, file)
            file.write('\n')


if __name__ == "__main__":
    init()
