import json
import os

import load_keywords


def split_in_steps(steps):

    # delete old
    for step in steps:
        path = f'output/websites_extracted_{step}.jsonl'
        if os.path.exists(path):
            os.remove(path)

    extracted = []
    with open('output/websites_extracted.jsonl', 'r') as f:
        for line in f:
            extracted.append(json.loads(line))

    all_bundled, url_bundled = [], []
    current_url = extracted[0]['start_url']
    for i in range(len(extracted)):

        if extracted[i]['start_url'] == current_url:
            url_bundled.append(extracted[i])

        else:
            current_url = extracted[i]['start_url']
            all_bundled.append(url_bundled)
            url_bundled = [extracted[i]]

    all_bundled.append(url_bundled)

    for bundle in all_bundled:

        counter = 0
        for step in steps:
            if counter < len(bundle):
                save_split(bundle[counter], step)
                counter += 1
            else:
                save_split(bundle[-1], step)


def transform_websites(size, level):

    # delete old
    path = f'output/websites_extracted_{level}_{size}.jsonl'
    if os.path.exists(path):
        os.remove(path)

    extracted = []
    with open(f'output/websites_extracted_{size}.jsonl', 'r') as file:
        for line in file:
            extracted.append(json.loads(line))

    keywords_categories = ['mobile_app', 'crm_system', 'digital_technologies',
                           'digital_products', 'digital_distribution', 'digital_working']
    keywords = load_keywords.load_keywords(keywords_categories)
    domains = load_keywords.load_keywords(['social_media'])

    for entry in extracted:

        # structure
        for key in list(entry['structure'].keys()):
            if key != 'size':
                entry['structure'][key] = entry['structure'][key] / entry['structure']['size']
        entry['structure']['size'] = entry['structure']['size'] / size

        # Social Media
        new = []
        for domain in entry['urls']['social_media']:
            if domain in domains and domains[domain][level] not in new:
                new.append(domains[domain][level])
        entry['urls']['social_media'] = new

        # languages
        total = 0
        languages = {'de': 0, 'en': 0, 'others': 0}
        for key, value in entry['languages'].items():
            if key in ['de', 'en']:
                languages[key] = value
            else:
                languages['others'] += value

            total += value

        if total > 0:
            for key, value in languages.items():
                languages[key] = value / total

        entry['languages'] = languages

        # keywords
        new = {category: [] for category in keywords_categories}
        for category in keywords_categories:
            for keyword in entry['keywords'][category]:
                if keyword in keywords and keywords[keyword][level] not in new[category]:
                    new[category].append(keywords[keyword][level])

        entry['keywords'] = new

        save_transform(entry, path)


def save_transform(element, path):

    with open(path, 'a') as file:
        json.dump(element, file)
        file.write('\n')


def save_split(data, step):

    with open(f'output/websites_extracted_{step}.jsonl', 'a') as file:
        json.dump(data, file)
        file.write('\n')


def modify_output(steps, levels=None):

    if levels is None:
        levels = ['word', 'id', 'cluster']

    split_in_steps(steps)
    for step in steps:
        for level in levels:
            transform_websites(step, level)


if __name__ == "__main__":

    steps_list = [25, 50, 100, 200, 250]  # depends on extraction process
    modify_output(steps_list)
