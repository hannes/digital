import folium
from folium.plugins import HeatMap, MarkerCluster
import pandas as pd

from database.read import Connection


def load_data():

    connector = Connection()

    data = {'duns_id': [], 'start_url': [], 'latitude': [], 'longitude': [], 'classification': [], 'regression': []}
    geocodes = connector.get_geocodes()
    for geocode in geocodes:
        if geocode['saxony']:
            company = connector.get_companies_by_duns(geocode['duns_id'])
            if company and company[0]['duns_id'] not in data['duns_id']:
                classified = connector.get_classified(company[0]['start_url'])
                if classified:
                    data['duns_id'].append(company[0]['duns_id'])
                    data['start_url'].append(company[0]['start_url'])
                    data['latitude'].append(geocode['latitude'])
                    data['longitude'].append(geocode['longitude'])
                    data['classification'].append(classified[0]['classification'])
                    data['regression'].append(classified[0]['regression'])

    return pd.DataFrame(data)


def basemap():

    data = load_data()

    # initialize map
    m = folium.Map(location=data[['latitude', 'longitude']].mean(), tiles=None)

    # base map
    base_map = folium.FeatureGroup(name='Basemap', overlay=True, control=False)
    base_map.add_child(folium.TileLayer(tiles='OpenStreetMap'))
    m.add_child(base_map)

    # Saxony
    style = {'color': '#676767', 'fillColor': '#00000000'}
    m.add_child(folium.GeoJson(open('saxony.geojson', encoding='utf-8').read(),
                               name='Saxony Borders', control=False, style_function=lambda x: style))

    # add bounds
    sw = data[['latitude', 'longitude']].min().values.tolist()
    ne = data[['latitude', 'longitude']].max().values.tolist()
    m.fit_bounds([sw, ne])

    return data, m


# heatmap with weights
def heat():

    for method in ['classification', 'regression']:

        data, m = basemap()

        if method == 'classification':
            degrees = [1, 2, 3, 4]
        else:
            degrees = [0, 0.25, 0.5, 0.75]

        for degree in degrees:
            if method == 'classification' and degree == max(degrees):
                name = f'Digitalisierungsgrad {degree}'
            else:
                name = f'Digitalisierungsgrad ab {degree}'

            feature_group = folium.FeatureGroup(name=name, show=False, overlay=False)

            # filter by level
            relevant = data.loc[data[method] >= degree]
            weights = relevant[method].tolist()

            if method == 'classification':
                weights_adjusted = []
                for weight in weights:
                    if weight == 1:
                        weights_adjusted.append(0.125)
                    elif weight == 2:
                        weights_adjusted.append(0.375)
                    elif weight == 3:
                        weights_adjusted.append(0.625)
                    else:
                        weights_adjusted.append(0.875)
                weights = weights_adjusted

            weighted = zip(relevant['latitude'].tolist(), relevant['longitude'].tolist(), weights)

            feature_group.add_child(HeatMap(weighted, radius=15))
            m.add_child(feature_group)

        # add layer control and save map
        m.add_child(folium.LayerControl(collapsed=False))
        m.save(f'maps/heat_{method}.html')


# marker map with top 100 for regression
def marker():

    # add markers
    def add_cluster(markers, map_name):

        feature_group = folium.FeatureGroup(name=map_name, show=False, overlay=False)

        m_marker = MarkerCluster()
        for i in range(len(markers)):
            m_marker.add_child(folium.Marker(location=[markers.iloc[i]['latitude'], markers.iloc[i]['longitude']],
                                             popup=f'<a href={markers.iloc[i]["start_url"]} target="_blank">'
                                                   f'{markers.iloc[i]["start_url"]}</a>'))
        feature_group.add_child(m_marker)
        return feature_group

    for method in ['classification', 'regression']:

        data, m = basemap()

        if method == 'classification':
            degrees = [1, 2, 3, 4]
        else:
            degrees = [(0, 0.25), (0.25, 0.5), (0.5, 0.75), (0.75, data['regression'].max())]

        for degree in degrees:

            if method == 'classification':
                name = f'Digitalisierungsgrad {degree}'

            else:
                if degree[0] == degrees[0][0]:
                    name = f'Digitalisierungsgrad kleinergleich {degree[1]}'
                elif degree[1] == degrees[-1][1]:
                    name = f'Digitalisierungsgrad größer {degree[0]}'
                else:
                    name = f'Digitalisierungsgrad größer {degree[0]} und kleinergleich {degree[1]}'

            # filter by digital level
            if method == 'classification':
                relevant = data.loc[data[method] == degree]
            else:
                relevant = data.loc[(degree[0] < data[method]) & (data[method] < degree[1])]

            m.add_child(add_cluster(relevant, name))

        if method == 'regression':
            top_100 = data.nlargest(100, 'regression')
            m.add_child(add_cluster(top_100, 'Top 100'))

        # add layer control and save map
        m.add_child(folium.LayerControl(collapsed=False))
        m.save(f'maps/marker_{method}.html')


if __name__ == "__main__":

    heat()
    marker()
