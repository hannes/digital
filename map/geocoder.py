from geopy.geocoders import Bing
import json
import os
import pandas as pd


# get geocodes of companies using Bing Maps API
def geocode_companies():

    key = 'secret'  # insert API key here
    geolocator = Bing(key)

    """
        Index | Col | Meaning
        0     | A   | D&B D-U-N-S®-Nummer
        1     | B   | Postleitzahl + Ort (postal code and location)
        2     | C   | Straße + Hausnummer (street and number)
    """

    directory = f'{os.path.dirname(os.getcwd())}/crawler/resources'
    companies = pd.read_excel(f'{directory}/data_companies.xls', usecols='A,B,C')
    companies = companies.values.tolist()

    for company in companies:
        address = f'{company[2]}, {company[1]}'

        location = geolocator.geocode(address)
        la = location.latitude
        lo = location.longitude

        entry = {'duns_id': company[0], 'latitude': la, 'longitude': lo, 'saxony': is_saxony(la, lo)}

        with open(f'geocodes_bing_companies.jsonl', 'a', encoding='utf-8') as file:
            json.dump(entry, file)
            file.write('\n')


# borders of Saxony
def is_saxony(latitude, longitude):

    # Saxony: https://sachsen-net.com/sachsen/zahlen
    latitude_min = 50.1719444
    latitude_max = 51.6852778
    longitude_min = 11.872777777777777
    longitude_max = 15.043611111111112

    if latitude_min <= latitude <= latitude_max and longitude_min <= longitude <= longitude_max:
        return True
    else:
        return False


if __name__ == "__main__":
    geocode_companies()
